﻿using System;
using System.Collections.Generic;
using System.Threading;

public class Program
{
    public static void PrintSentence(List<String> stringList1)
    {
        foreach (string word in stringList1)
        {
            Console.Write(" " + word);
            Thread.Sleep(2000);
        }

        Console.WriteLine("");
    }

    static void Main()
    {

        List<String> stringList1 = new List<string>() {
            "Una", "vez", "habia", "un", "gato"};
        List<String> stringList2 = new List<string>() {
            "en", "un", "lugar", "de", "la", "mancha"};
        List<String> stringList3 = new List<string>() {
            "once","upon", "a", "time", "in", "the", "west"};

        Thread thread1 = new Thread(() => {
            PrintSentence(stringList1);
        });

        Thread thread2 = new Thread(() => {
            thread1.Join();
            PrintSentence(stringList2);
        });

        Thread thread3 = new Thread(() => {
            thread2.Join();
            PrintSentence(stringList3);
        });

        /*  thread1.Start();
    thread2.Start();
    thread3.Start();*/


        Nevera nevera = new Nevera(0);

        Thread threadAnitta = new Thread(() => {
            nevera.llenarNevera("Anitta");
        });

        Thread threadBadBunny = new Thread(() =>
        {
            threadAnitta.Join();
            nevera.beberCerveza("Bad Bunny");
        });

        Thread threadLilNasX = new Thread(() => {
            threadBadBunny.Join();
            nevera.beberCerveza("Lil Nas X");
        });

        Thread threadManuelTurizo = new Thread(() => {
            threadLilNasX.Join();
            nevera.beberCerveza("Manuel Turizo");
        });


        /*threadAnitta.Start();
        threadBadBunny.Start();
        threadLilNasX.Start();
        threadManuelTurizo.Start();*/
    }

}

public class Nevera
{
    int numCervezas;

    public Nevera(int n)
    {
        numCervezas = n;
    }

    public void llenarNevera(string persona)
    {
        Random random = new Random();
        int numNuevas = random.Next(0, 7);
        numCervezas += numNuevas;
        if (numCervezas > 9)
        {


            numCervezas = 9;
        }

        Console.WriteLine(persona + "ha llenado la nevera con " + numNuevas + " cervezas, hay " + numCervezas);
    }

    public void beberCerveza(string persona)
    {
        Random random = new Random();
        int numBebidas = random.Next(0, 7);
        numCervezas -= numBebidas;
        Console.WriteLine(persona + "ha bebido " + numBebidas + " cervezas.");
    }
}
